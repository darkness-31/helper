# Помощник

## ⚠

```bash
     apt-get update && sudo apt-get upgrade -y 
```

---

## REPOS

```bash
# 64-bit Original
rpm [p10] http://ftp.altlinux.org/pub/distributions/ALTLinux p10/branch/x86_64 classic
rpm [p10] http://ftp.altlinux.org/pub/distributions/ALTLinux p10/branch/noarch classic
rpm [p10] http://ftp.altlinux.org/pub/distributions/ALTLinux p10/branch/x86_64-i586 classic

# 64-bit Yandex
rpm [p10] http://mirror.yandex.ru/altlinux p10/branch/x86_64 classic
rpm [p10] http://mirror.yandex.ru/altlinux p10/branch/noarch classic
rpm [p10] http://mirror.yandex.ru/altlinux p10/branch/x86_64-i586 classic
```

> Директория REPOSITORY RPM \
> /etc/apt/source.list.d

```bash
# Добавить в директории Репозитории Яндекса
echo "rpm [p10] http://mirror.yandex.ru/altlinux p10/branch/x86_64 classic \
      rpm [p10] http://mirror.yandex.ru/altlinux p10/branch/x86_64-i586 classic \
      rpm [p10] http://mirror.yandex.ru/altlinux p10/branch/noarch classic " \
      > /etc/apt/source.list.d/yandex.list
```

---

## VIM

> Перемещение по окнам \
> **< C - { H, J, K, L } >**  

> Переместить окно\
> **< C-W > + { H, J, K, L }**

```vim
vert
vim -O {$file1, $file2, $dir1, ...}
horz
vim -o {$file1, $file2, $dir1, ...}

:sp [$file, $dir]
:vsp [$file, $dir]

:w [$name]

:q
:qall
```

---

## VIRTUAL SWITCH
```bash
# Включить openvswitch
systemctl enable --now openvswitch

# Создать коммутатор
ovs-vsctl add-br $name_switch

# Добавить интерфейсы
ovs-vsctl add-port $name_switch $name_interface
```

---

## LVM

```bash
# Для установки LVM
 apt-get install -y lvm2

# создать диск
# pvcreate /dev/{sdc, sdb}
pvcreate /dev/{$disk, ...} 

# создать группу из дисков
# vgcreate groupDisks /dev/{sdb, sdc}
vgcreate $name /dev/{$disk, ...} 

# создать логический том
# lvcreate -L 1G groupDisks 
# lvcreate -L 50G -n logicTom01 groupDisks 
# lvcreate -l 100%{VG,FREE} -n logicTom01 groupDisks 
lvcreate {option} $name $group

#striped
# lvcreate -i2 -I64 -L20G -n strippedtom groupDisks
lvcreate -i[$count_disk] -I[$size, default=64] -L[$size_group_disks] -n $name $group

pvdisplay # отобразить созданые диски
vgdisplay # отобразить группы дисков 
lvdisplay # отобразить созданые логические диски

# Зашифровть диск
cryptsetup luksFormat /dev/$disk
```

---

## FDISK

```bash
# просмотреть диски
fdisk -l

# редактировать диск 
fdisk /dev/{$disk, ...}

# отформатировать диск
mkfs.{ntfs,ext{2,3,4}} /dev/$disk
mkfs.fat -f {16,32} /dev/$disk

# размонтировать\монтировать дисков
{umount,mount} /dev/$disk /$dir

# размонтировать всё
umount -a
```

### Автоматическое монтирование

#### FSTAB

> File \
> /etc/fstab

```bash
# Диск      Куда    Тип     Опции       Резерв  Проверка
# /dev/sda  /mnt    ext4    default     0       0
$disk $dir $type_disk {option} [0,1] [0,1,2]
```

#### SYSTEMMD

> Directory \
> /etc/systemd/system/

```bash
[Unit]
Description=$

[Mount]
What=$disk
Where=$dir
Type=$type_disk
Options={option}

[Install]
WantedBy=multi-user.target
```

---

## POSTGRES

```bash
# Для установки postgres
 apt-get install -y postgresql15-server

# перезапустить\запустить\остановить автозапуск после загрузки
 systemctl {restart,start,stop} postgresql
 service postgresql {restart,start,stop}

# Включить\выключить автозапуск после загрузки
 systemctl {enable,disable} postgresql
 chkconfig postgresql {on,off}

# Добавить в автозагрузку
 systemctl enable --now postgresql

#создать базу
 pgbanch -i $name_db
```

### postgresql.conf

```postgresql
listen_addresses = '*'
```

### pg_hba.conf

```postgresql
host {user, all} {db, all}    {::1/32, all} md5
```

```bash
# работа под администратором
 psql -U postgres

# Создать пользователя
createuser -U postgres --no-superuser --no-createdb --no-createrole --encrypted --pwprompt $name

# cоздание базы данных
createdb -U postgres -O [user] [db]

# просмотр баз данных 
psql -U postgres -c "\l+"
```

### PostgresPro

```bash
echo rpm "http://repo.postgrespro.ru/pg_probackup/rpm/latest/altlinux-p9 x86_64 vanilla" > /etc/apt/sources.list.d/pg_probackup.list 

 apt-get update
 apt-get install pg_probackup-version
```

### REPLICATION MASTER

```bash
# Создать пользователя repluser
su - postgres -s /bin/bash -c "createuser --replication -P repluser"

# Показать где находится конфигурационный файл
su - postgres -c "psql -c 'SHOW config_file;'"
```

> Конфигурациооный postgresql.conf 
> /var/lib/pgsql/data/postgresql.conf

+ **listen_addresses** = '*, $master_ip'
+ **wal_level** = replica
+ **max_wal_senders** = 2
+ **max_replication_slots** = 2
+ **hot_standby** = on
+ **hot_standby_feedback** = on

> Конфигурациооный postgresql.conf 
> /var/lib/pgsql/data/pg_hba.conf

+ host replication repluser ::1/32 md5
+ host replication repluser $master_ip/32 md5
+ host replication repluser $repl_ip/32 md5

### REPLICATION SLAVE

```bash
# Путь до конфигов
su - postgres -c "psql -c 'data_directory;'"

systemctl stop postgresql

tar -czvf /tmp/pg.bak.tar.gz /var/lib/pgsql/data
rm -rf /var/lib/pgsql/data/*
su - postgres -c "pg_basebackup --host=$master_ip --username=repluser --pgdata=/var/lib/pgsql/data --wal-method=stream --write-recovery-conf"
```

> Конфигурациооный postgresql.conf 
> /var/lib/pgsql/data/postgresql.conf

+ listen_addresses = 'localhost, $slave_ip'

```bash
# Проверка на репликацию
# На MASTER
su - postgres -s /bin/bash -c "psql -c 'SELECT * FROM pg_stat_replication;'"
# На SLAVE
su - postgres -s /bin/bash -c "psql -c 'SELECT * FROM pg_stat_wal_receiver;'"
```
---

## Zabbix

```bash
# Установка
apt-get install -y postgresql15-server zabbix-server-pgsql fping

# Создание системных бд 
/etc/init.d/postgresql initdb

# Создать пользователя zabbix
su - postgres -s /bin/sh -c 'createuser --no-superuser --no-createdb --no-createrole --encrypted --pwprompt zabbix'
# ⚠ пароль

# Создать базу для Zabbix
su - postgres -s /bin/sh -c 'createdb -O zabbix zabbix'
su - postgres -s /bin/sh -c 'psql -U zabbix -f /usr/share/doc/zabbix-common-database-pgsql-*/schema.sql zabbix'
su - postgres -s /bin/sh -c 'psql -U zabbix -f /usr/share/doc/zabbix-common-database-pgsql-*/images.sql zabbix'
su - postgres -s /bin/sh -c 'psql -U zabbix -f /usr/share/doc/zabbix-common-database-pgsql-*/data.sql zabbix'
```

### PHP, APACHE2
```bash
# Установка пакетов
apt-get install -y php7 php7-{mbstring,sockets,gd2,xmlreader,pgsql,ldap}

# автозагрузка APACHE
systemctl enable --now httpd2
```

> Конфигурация PHP \
> /etc/php/7.4/apache2-mod_php/php.ini 

- **memory_limit** = 256M
- **post_max_size** = 32M
- **max_execution_time** = 600
- **max_input_time** = 600
- **date.timezone** = Europe/Moscow
- **always_populate_raw_post_data** = -1

> Конфигурация Zabbix\
> /etc/zabbix/zabbix_server.conf

- **DBHost** = localhost
- **DBName** = zabbix
- **DBUser** = zabbix
- **DBPassword** = $password

```bash
# Добавить в автозагрузку Zabbix
systemctl enable --now zabbix_pgsql
```

### Веб-интерфейс
```bash
# Установить пакет
apt-get install -y zabbix-phpfrontend-apache2-mod_php7 zabbix-phpfrontend-php7

# Ссылка на конфигурацию файл веб-сервера
ln -s /etc/httpd2/conf/addon.d/A.zabbix.conf /etc/httpd2/conf/extra-enabled/
```

### Агент
```bash
# Установка пакета
apt-get install -y zabbix-agent

# Добавить в автозагрузку
systemctl enable --now zabbix-agent
```

> Конфигурация Zabbix Agent 
> /etc/zabbix/zabbix_agentd.conf

- Server = $ip_zabbix_server
- ServerActive = $ip_zabbix_server
- Hostname = $hostname

---

## vESR

> **Login: Admin**\
> **Password: password**

Сокращения:

+ do - привилегированный режим
+ sh - show
+ int - interfaces
+ config - configure terminal
+ gigabitethernet - gi

| Конфигурации | Описания |
| ------------ | -------- |
| `system:factory-config` | Заводской конфигурационный файл |
| `system:default-config` | Пустой конфигурационный файл |
| `system:resore-config` | Старый `system:running-config` после применения `commit` |
| `system:running-config` | Текущий файл конфигурации |
| `system:canditate-config` | Временый файл приминямой конфигурации |

| Команда | Описание |
| ------- | -------- |
| <?> | Помощь |
| `do`  | Выполнить команду привилегированного режима |
| `exit` | выйти из среды |
| `end` | выйти в глобальную конфигурацию |
| `commit` | Подтвердить изменение |
| `confirm` | Сохранение подтверждённых изменений |
| `roolback` | `running-config` -> `candidate-config` |
|            |    Отменить `candidate-config` |
| `restore` | `restore-config` -> `running-config` -> `candidate-config` |
|            |   Отменить `commit` |
| `save` | Сохранение `candidate-config` |
| `config` | Перейти в конфигурацию |
| `ip ssh server` | включить SSH сервер |
| `ip dhcp-server` | Включить DHCP сервер |
| `sh ip dhcp binding` | Просмотр занятый IP выданым DHCP сервером|
| `sh int status` | Просмотр статус портов |
| `sh ip int` | Просмотр ip адресса на портах |
| `sh running-config` | Просмотр всей конфигурации |
| `sh candidate-config` | Просмотр предпологаемой конфигурации, которая не работает с системой |
| `reload system` | Перезагрузка системы |

| Среда | Команда | Значение | Описание |
| ------- | ------- | -------- | -------- |
| # | `ping` | Адресс | Проверка качества соединения |
| # | `password` | Любое | Изменить пароль |
| # | `copy` | `system:$config_name system:$config_name` | Копирования конфигурации |
| config | `hostname` | Любое | Изменить название сервера |
| config | `int gi` | `$/$/$` | Перейти по порту |
| gi| `ip address` |`dhcp`\\`$.$.$.$/$`| Изменить у порта ip адресс  |
| gi | `ip firewall` | `enable`\\`disable` | Межсетевой экран |
| gi | `mode` | `switchport`\\`routerport` | Изменить режим работы |
| gi | `description` | `"$"` | Описание порта |
| config | `username` | Любое | Создать пользователя |
| config-user | `password encrypted` | Любое | Зашифрованный пароль |
| config-user | `privilege` | 1-15 | Настроить доступ для пользователя |
| config | `ip dhcp-server pool` | Любое | Создания DHCP сервер с его названием |
| config-dhcp-server | `network` | `$.$.$.$/$` | Указать сеть в которой будет работать |
| config-dhcp-server | `address-range` | `$.$.$.$-$.$.$.$` | Выдача адресов клиентам |
| config-dhcp-server | `default-router` | `$.$.$.$` | Шлюз по-умолчанию |
| config-dhcp-server | `dns-server` | `$.$.$.$,$.$.$.$` | Днс по-умолчанию |
| config-dhcp-server | `domain-name` | Любое | Выдача домена |

### DHCP

> Настройка DHCP сервера на виртуальном коммутаторе

1. `config`
2. `int gi$/$/$`
    > Интернетный порт
    1. `description WAN`
    2. `ip address dhcp`
3. `int gi$/$/$`
    > Локальный порт
    1. `description LAN`
    2. `ip address $.$.$.$/$`
4. `security zone trusted`
    > Создаём зону безопасности с именем `trusted`
5. `int gi$/$/$`
    1. `security-zone trusted`
6. `ip dhcp-server pool $name_dhcp`
    1. `network $.$.$.$/$`
    2. `address-range $.$.$.$-$.$.$.$`
    3. `excluded-address-range $.$.$.1`
        > Исключаем выдачу первого адреса из выдачи
    4. `excluded-address-range $.$.$.254`
        > Исключаем выдачу последнего адреса из выдачи
    5. `default-router $.$.$.$`
        > Шлюз интерфеса локального порта
    6. `default-server $.$.$.$`
        > DNS сервер
7. `object-group service dhcp_server`
    1. `post-range 67`
8. `object-group service dhcp_client`
    1. `post-range 68`
9. `security zone-pair trusted self`
    > Создаёться правило безопасности для прхождения UDP пакетов
    1. `rule 1`
        1. `match protocol udp`
        2. `match source-port dhcp_client`
            > Откуда
        3. `match destination-port dhcp_server`
            > Куда
        4. `action permit`
        5. `enable`
10. `ip dhcp-server`
11. `do sh ip dhcp server pool $name_dhcp`
    > прсомотр параметров у DHCP сервера

### NAT

> Настройка доступа к интернету

1. `config`
2. `int gi$/$/$`
    > Интернетный порт
    1. `description WAN`
    2. `ip address dhcp`
3. `int gi$/$/$`
    > Локальный порт
    1. `description LAN`
    2. `ip address $.$.$.$/$`
4. `security zone trusted`
5. `security zone untrusted`
6. `int gi$/$/$`
    > Локальный порт
    1. `security-zone trusted`
7. `int gi$/$/$`
    > Интернетный порт
    1. `security-zone untrusted`
8. `object-group network LAN`
    1. `ip address-range $.$.$.$-$.$.$.$`
9. `obnject-group newtork WAN`
    1. `ip address-range $.$.$.$`
10. `security zone-pair trusted untrusted`
    > Разрешение пропуска трафика в интернет
    1. `rule 1`
        1. `match source-address LAN`
        2. `action permit`
        3. `enable`
11. `net source`
    1. `pool WAN`
        1. `ip aaddress-range $.$.$.$`
            > IP-адрес смотрящий в сеть интернет
12. `ruleset SNAT`
    > Правила применяются только для пакетов, направляющихся в публичную сеть
    1. `to zone untersted`
        1. `rule 1`
            1. `match source-address LAN`
            2. `action source-nat pool WAN`
            3. `enable`
13. `do sh ip nat translation`

### ANSIBLE

```bash
# установка ansible
apt-get install ansible

# Генерируем ключ
ssh-keygen -t ed25519

# Копирования ключа на другой сервер
ssh-copy-id -i $path_key $user_ssh@$ip_ssh

# Создать группу ip 
vim /etc/ansible/hosts
```

> Пример группировки ip в ansable



```ini
[network]
192.168.100.1
192.168.100.2
192.168.100.3

[servers]
192.168.2.11
192.168.2.12
```



> Пример playbook

```yaml
- hosts: all
  user: root
  tasks:
    - name: delete the file if exists
      file:
        path: /tmp/output.yaml
        state: absent
      delegate_to: localhost
    - name: get data to a file
      lineinfile:
        dest: /tmp/output.yaml
        create: yes
        line: "{{hostvars[inventory_hostname].ansible_hostname}} - {{hostvars[inventory_hostname].ansible_all_ipv4_addresses}}"
      delegate_to: localhost
```

### DOCKER

```yaml
- hosts: test_servers
  user: root
  tasks:
    - name: install dependencies
      apt_rpm:
        name: "{{item}}"
        state: present
        update_cache: yes
      loop:
        - apt-https
        - ca-certificates
        - curl
        - gnupg2
    - name: install docker
      apt_rpm:
        name: docker-engine
        state: present
        update_cache: yes
    - name: service active
      service:
        name: docker
        state: started
        enabled: yes
    - name: ensure group docker exists
      ansible.builtin.group:
        name: docker
        state: present
    - name: adding user to docker group
      user:
        name: user
        groups: docker
        append: yes
    - name: Install docker-compose
      get_url:
        url: https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64
        dest: /usr/local/bin/docker-compose
        mode: 'u+x,g+x'
    - name: Change file ownership, group and permissions
      ansible.builtin.file:
        path: /usr/local/bin/docker-compose
        owner: user
        group: user

```